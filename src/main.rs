use std::fmt::Formatter;
use std::{fmt, io, process, vec, env};
use std::fs::File;
use std::io::Read;
use rand::{Rng};
use dotenv::dotenv;

struct Hangman {
    word_to_guess: String,
    lives: u8,
    guessed: LetterList,
}
struct LetterList {
    characters: Vec<char>,
}

impl LetterList {
    fn new(characters: Vec<char>) -> Self {
        LetterList { characters }
    }
}

impl Hangman {
    fn new(word_to_guess: &str) -> Self {
        Hangman {
            word_to_guess: word_to_guess.trim().to_string(),
            lives: 10,
            guessed: LetterList::new(vec!['_'; word_to_guess.trim().chars().count()]),
        }
    }
    fn guess(&mut self, character: char) {
        if self.word_to_guess.contains(character) {
            self.reveal_character(character);
            return;
        }
        self.lives = self.lives - 1;
    }
    fn reveal_character(&mut self, character: char) {
        for i in 0..self.word_to_guess.len() {
            if self.word_to_guess.chars().nth(i) == Some(character) {
                self.guessed.characters[i] = character;
            }
        }
    }
}
impl fmt::Display for LetterList {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.characters.iter().fold(Ok(()), |result, letter| {
            result.and_then(|_| write!(f, "{} ", letter))
        })
    }
}

fn main() {
    dotenv().ok();
    let word: String = startup();
    let mut game = Hangman::new(&word);
    while game.lives > 0 && game.guessed.characters.contains(&'_') {
        process::Command::new("clear").status().unwrap();
        println!("lives:{}, ", game.lives);
        println!("word: {}", game.guessed);
        game.guess(ask_character());
    }
    if game.lives == 0 {
        println!(
            "Defeat, the word you where looking for was: {}",
            game.word_to_guess
        );
    } else {
        println!("Victory, you fount the word: {}", game.word_to_guess);
    }
}

fn startup() -> String {
    let mut input = String::new();
    println!("Give a word to start hangman (or just press enter if you want to get a random word)");
    io::stdin()
        .read_line(&mut input)
        .expect("failed to read line");
    if input == "\n" {
        input = get_word_from_file();
    }
    process::Command::new("clear").status().unwrap();
    input
}

fn ask_character() -> char {
    let mut input = String::new();
    println!("Give a letter to try");
    io::stdin()
        .read_line(&mut input)
        .expect("failed to read line");
    let characters: Vec<char> = input.chars().collect();
    characters[0]
}

fn get_word_from_file() -> String{
    let mut file = File::open(env::var("WORDLIST")
        .expect("Could not read env file"))
        .expect("Could not open file");
    let mut words = String::new();
    file.read_to_string(&mut words).expect("An error occured while reading the file");
    let available_words: Vec<&str> = words.trim().split('\n').collect();
    let random_index = rand::thread_rng().gen_range(0..available_words.len());
    String::from(available_words[random_index])
}


